FROM alpine:3.16

RUN apk add --no-cache nginx npm sassc
RUN npm install -g minify pug ts-node typescript uglify-js
RUN mkdir /app

ARG _UID
ARG _GID
ARG _UNAME
ARG _GNAME
ENV _UID=${_UID:-1000}
ENV _GID=${_GID:-1000}
ENV _UNAME=${_UNAME:-user}
ENV _GNAME=${_GNAME:-user}

RUN addgroup -g ${_GID} ${_GNAME} \
  && adduser -u ${_UID} -G ${_GNAME} -s /bin/bash -h /home/${_UNAME} -D ${_UNAME}

RUN mkdir -p /usr/share/nginx/html 2> /dev/null

RUN sed -i "/user nginx;/d" /etc/nginx/nginx.conf
RUN chown -R ${_UNAME}:${_UNAME} /var/lib/nginx
RUN chown -R ${_UNAME}:${_UNAME} /var/log/nginx
RUN chown -R ${_UNAME}:${_UNAME} /run/nginx
RUN chown -R ${_UNAME}:${_UNAME}  /usr/share/nginx/html

EXPOSE 8080
STOPSIGNAL SIGQUIT

WORKDIR /app
USER ${_GID}:${_UID}

RUN touch /var/lib/nginx/logs/error.log
RUN touch /var/log/nginx/error.log
RUN touch /run/nginx/nginx.pid

CMD ["nginx", "-g", "daemon off;"]
